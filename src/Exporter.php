<?php

/**
  * Copyright (C) 2019, raphaël.droz+floss@gmail.com
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  */

namespace Homepage;

class Exporter {
	public static $html = false;
	private static $news, $posts, $events, $menu_items;

	public function __construct() {
		self::$news = [
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => 3,
			'fields'         => 'ids',
			'tax_query'      => [
				[
					'taxonomy' => 'post-subtype',
					'field'    => 'slug',
					'terms'    => 'news',
					'operator' => 'IN',
				],
			],
		];

		self::$posts = [
			'post_type'      => 'post',
			'post_status'    => 'publish',
			'posts_per_page' => 3,
			'fields'         => 'ids',
			'tax_query'      => [
				[
					'taxonomy' => 'post-subtype',
					'field'    => 'slug',
					'terms'    => 'news',
					'operator' => 'NOT IN',
				],
			],
		];

		self::$events = [
			'post_type'      => 'event',
			'post_status'    => 'publish',
			'posts_per_page' => 6,
			'fields'         => 'ids',
			'meta_query'     => [
				'relation' => 'OR',
				// check to see if end date has been set
				[
					'key'     => 'end_date',
					'value'   => date( 'Ymd' ),
					'compare' => '>=',
					'type'    => 'date',
				],
				// if no end date has been set use start date
				[
					'key'     => 'date',
					'value'   => date( 'Ymd' ),
					'compare' => '>=',
					'type'    => 'date',
				],
			],
		];

		self::$menu_items = [
			'post_type'      => 'nav_menu_item',
			'post_status'    => 'publish',
			'posts_per_page' => -1,
			'fields'         => 'ids',
		];
	}


	static function dump( $destination ) {
		if ( ! class_exists( '\WP_Export_WXR_Formatter' ) ) {
			\WP_CLI::error( "Can't find WP_Export_WXR_Formatter (wp export command)" );
			return false;
		}
		$ref1 = ( new \ReflectionClass( '\WP_Export_WXR_Formatter' ) )->getFileName();
		$ref2 = ( new \ReflectionClass( '\Export_Command' ) )->getFileName();
		\WP_CLI::line( "Load export-command from $ref1" );
		\WP_CLI::line( "                     and $ref2" );

		$dumper = new self();
		return $dumper->create_whole_zip( $destination );
	}

	function get_home_html() {
		if ( self::$html ) {
			return self::$html;
		}
		$response = wp_remote_get( home_url() );
		if ( is_wp_error( $response ) ) {
			throw(new \Exception( "Can't fetch homepage" ));
		}
		return ( self::$html = $response['body'] );
	}

	static function find_files( $str ) {
		$arr = [];
		preg_match_all( "!app/uploads/\K([^\"' )]+)!", $str, $arr );
		$arr = array_map(
			function( $e ) {
					return preg_replace( '/-[0-9]{3,4}x[^.]+/', '', $e );
			},
			$arr[1]
		);
		return array_values( array_unique( $arr ) );
	}

	function get_file_ids( $files ) {
		global $wpdb;
		$s = implode( ', ', array_fill( 0, count( $files ), '%s' ) );
		$q = $wpdb->prepare(
			"SELECT DISTINCT post_id FROM {$wpdb->postmeta} "
						. " WHERE meta_key = '_wp_attached_file' AND meta_value IN ($s)",
			$files
		);
		return $wpdb->get_col( $q );
	}

	function get_posts_id_from_menu_items() {
		global $wpdb;
		return $wpdb->get_col(
			"SELECT m2.meta_value as c FROM {$wpdb->postmeta} m1 "
						  . " LEFT JOIN {$wpdb->postmeta} m2 ON (m1.post_id = m2.post_id)"
						  . " WHERE m1.meta_key = '_menu_item_type' AND m1.meta_value = 'post_type'"
						  . " AND m2.meta_key = '_menu_item_object_id'"
		);
	}

	function export_ids( $ids,
					  $with_attachments = false,
					  $opts = [ 'header', 'categories', 'tags', 'nav_menu_terms', 'custom_taxonomies_terms', 'rss2_head_action' ] ) {
		$args = [
			'with_attachments' => $with_attachments,
			'post_ids'         => $ids,
		];

		if ( ! function_exists( 'wp_export' ) ) {
			\Export_Command::load_export_api();
		}
		$formatter = new \WP_Export_WXR_Formatter( new \WP_Export_Query( $args ) );

		// Avoid site_metadata and authors
		$output = $formatter->before_posts( $opts );
		foreach ( $formatter->posts() as $post_in_wxr ) {
			$output .= $post_in_wxr;
		}
		$output .= $formatter->after_posts();
		return $output;
	}

	function export_deps() {
		// We wants attachments for all these contents
		$home_id        = (int) get_option( 'page_on_front' );
		$news_ids       = ( new \WP_Query( self::$news ) )->posts;
		$post_ids       = ( new \WP_Query( self::$posts ) )->posts;
		$events_ids     = ( new \WP_Query( self::$events ) )->posts;
		$menu_items_ids = ( new \WP_Query( self::$menu_items ) )->posts;

		$ids = [];
		array_push( $ids, $home_id, ...$news_ids, ...$post_ids, ...$events_ids, ...$menu_items_ids );
		$ids = array_values( array_unique( $ids ) );
		return $this->export_ids( $ids, $with_attachments = true );
	}

	function export_nodeps() {
		// We don't need attachments for those
		$ids       = [];
		$ref_posts = $this->get_posts_id_from_menu_items();
		$file_urls = $this->find_files( $this->get_home_html() );
		$file_ids  = $this->get_file_ids( $file_urls );

		array_push( $ids, ...$ref_posts, ...$file_ids );
		$ids = array_values( array_unique( $ids ) );
		return $this->export_ids( $ids, $with_attachments = false, $opts = [ 'header' ] );
	}

	function make_forms() {
		// ToDo
	}

	function options() {
		global $wpdb;
		$q = $wpdb->prepare(
			"SELECT option_name, option_value FROM {$wpdb->options} WHERE option_name REGEXP %s",
			[
				implode(
					'|',
					[
						'.?options_.*',
						'show_on_front',
						'page_on_front',
						'blogname',
						'blogdescription',
						'posts_per_page',
						'date_format',
						'time_format',
						'permalink_structure',
					]
				),
			]
		);

		$arr = array_map(
			function ( $e ) {
					return $e->option_value;
			},
			$wpdb->get_results( $q, OBJECT_K )
		);
		// Other data
		$other_data = [
			'__theme_mods'      => get_theme_mods(),
			'__front_page_name' => get_page( get_option( 'page_on_front' ) )->post_name,
		];
		return json_encode( array_merge( $arr, $other_data ) );

	}

	function tables( $tables ) {
		$ret = [];
		global $wpdb;
		foreach ( $tables as $table ) {
			if ( is_string( $table ) ) {
				$ret[ $table ] = $wpdb->get_results( "SELECT * FROM {$wpdb->{$table}}", ARRAY_A );
			} else { // array (table_name, prepared-query)
				$ret[ $table[0] ] = $wpdb->get_results( $table[1], ARRAY_A );
			}
		}
		return json_encode( $ret );
	}

	function nav_menu_terms() {
		global $wpdb;
		$term_taxonomy = $wpdb->prepare( "FROM {$wpdb->term_taxonomy} WHERE taxonomy = %s", [ 'nav_menu' ] );
		return $this->tables(
			[
				[ 'term_relationships', $wpdb->prepare( "SELECT * FROM {$wpdb->term_relationships} WHERE term_taxonomy_id IN (SELECT term_taxonomy_id FROM wp_term_taxonomy WHERE taxonomy = %s)", [ 'nav_menu' ] ) ],
				[ 'term_taxonomy', "SELECT * $term_taxonomy" ],
				[ 'terms', "SELECT * FROM {$wpdb->terms} WHERE term_id IN (SELECT term_id $term_taxonomy)" ],
			]
		);
	}

	function create_whole_zip( $destination = false ) {
		$upload_dir = wp_upload_dir()['basedir'];
		$zip        = new \ZipArchive();

		if ( $destination == '-' ) {
			$destination = false;
		}

		$stdout = ! is_string( $destination );
		if ( ! is_string( $destination ) ) {
			$destination = tempnam( sys_get_temp_dir(), 'dump' );
		}

		$res = $zip->open( $destination, \ZipArchive::CREATE | \ZipArchive::OVERWRITE );
		$zip->addFromString( 'export-deps.wxr', $this->export_deps() );
		$zip->addFromString( 'export-nodeps.wxr', $this->export_nodeps() );
		$zip->addFromString( 'options.json', $this->options() );
		$zip->addFromString( 'tables.json', $this->nav_menu_terms() );
		foreach ( $this->find_files( $this->get_home_html() ) as $f ) {
			$zip->addFile( $upload_dir . '/' . $f, $f );
		}
		$zip->close();

		if ( $stdout ) {
			readfile( $destination );
			unlink( $destination );
		}
		return true;
	}
}
