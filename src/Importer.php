<?php

/**
  * Copyright (C) 2019, raphaël.droz+floss@gmail.com
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  */

namespace Homepage;

class Importer {
	const options = [
		'prefill_existing_posts'    => true,
		'prefill_existing_comments' => true,
		'prefill_existing_terms'    => true,
		'update_attachment_guids'   => true,
		'aggressive_url_search'     => false,
		'fetch_attachments'         => true,
	];

	static function load( $source, $sleep = 5 ) {
		if ( ! class_exists( 'WXR_Importer' ) ) {
			\WP_CLI::error( "Can't find WXR_Importer" );
			return false;
		}

		\WP_CLI::warning( "loading dump in $sleep seconds..." );
		sleep( $sleep );
		$loader = new self();
		return $loader->load_whole_zip( $source );
	}

	function options( $content ) {
		$json = json_decode( $content, true );
		foreach ( $json as $k => $v ) {
			if ( strpos( $k, '__' ) === 0 ) {
				continue;
			}
			update_option( $k, $v );
		}
		foreach ( $json['__theme_mods'] ?? [] as $k => $v ) {
			set_theme_mod( $k, $v );
		}
	}

	function front( $content ) {
		$json = json_decode( $content, true );
		if ( isset( $json['__front_page_name'] ) ) {
			update_option( 'page_on_front', get_page_by_path( $json['__front_page_name'] )->ID );
		}
	}

	function tables( $content ) {
		$json = json_decode( $content, true );
		global $wpdb;
		foreach ( $json as $table => $rows ) {
			$wpdb->query( "TRUNCATE TABLE {$wpdb->{$table}}" );
			foreach ( $rows as $row ) {
				$q = $wpdb->prepare(
					"INSERT INTO {$wpdb->{$table}}"
							. ' (' . implode( ', ', array_keys( $row ) ) . ')'
							. ' VALUES (' . implode( ', ', array_fill( 0, count( $row ), '%s' ) ) . ')',
					array_values( $row )
				);
				$wpdb->query( $q );
			}
		}
	}

	public static function ignore_uncategorized_term( $data, $meta ) {
		// Exported as US, by import test for existence using UK name => added twice
		// => change imported ids => break menus (theme-mods) which depends on them.
		if ( $data['name'] == 'Uncategorized' || $data['name'] == 'Uncategorised' ) {
			return '';
		}
		return $data;
	}

	// From drupal-import plugins
	public static function process_attachment_uploaded( $uploaded, $remote_url, $post, $meta ) {
		$filepath = wp_upload_dir()['basedir'] . '/' . $post['upload_date'] . '/' . sanitize_file_name( basename( urldecode( $remote_url ) ) );
		if ( file_exists( $filepath ) && ( $filesize = filesize( $filepath ) ) ) {
			$wp_url = wp_upload_dir()['baseurl'] . '/' . $post['upload_date'] . '/' . urlencode( basename( $filepath ) );
			$id     = attachment_url_to_postid( $wp_url );
			if ( $id ) {
				// normally, post_exists() from WXR-importer would not even trigger the hook if attachment already exists.
				// still, implement the condition in case of
				$metadata        = wp_get_attachment_metadata( $id );
				$metadata['url'] = $wp_url;
				\WP_CLI::debug(
					sprintf(
						'unmanaged media file "%s" already registered (id: %d, %d bytes).',
						$post['post_title'],
						$id,
						$filesize
					)
				);
				return $metadata;
			} else {
				\WP_CLI::debug(
					sprintf(
						'unmanaged media file "%s" found on filesystem (%d bytes).',
						$post['post_title'],
						$filesize
					)
				);
				return [
					'file' => $filepath,
					'url'  => $wp_url,
				];
			}
		}
		return $uploaded;
	}

	function load_whole_zip( $source ) {
		$basedir = wp_upload_dir()['basedir'];

		$zip    = new \ZipArchive();
		$opened = $zip->open( $source );
		if ( $opened !== true ) {
			throw( new \Exception( "Archive $source read error ($opened)" ) );
		}
		$zip->extractTo( $basedir );
		for ( $i = 0; $i < $zip->numFiles; $i++ ) {
			touch( $basedir . '/' . $zip->statIndex( $i )['name'], $zip->statIndex( $i )['mtime'] );
		}
		$options = $zip->getFromName( 'options.json' );
		$tables  = $zip->getFromName( 'tables.json' );
		$zip->close();

		// Options first
		$this->options( $options );

		// Raw tables
		$this->tables( $tables );

		// Then import
		$importer          = new \WXR_Importer( self::options );
		$logger            = new \WP_Importer_Logger_CLI();
		$logger->min_level = 'debug';
		$importer->set_logger( $logger );

		add_filter( 'wxr_importer.process_attachment.uploaded', [ __CLASS__, 'process_attachment_uploaded' ], 10, 4 );
		add_filter( 'wxr_importer.pre_process.term', [ __CLASS__, 'ignore_uncategorized_term' ], 10, 2 );
		$importer->import( $basedir . '/export-deps.wxr' );
		$importer->import( $basedir . '/export-nodeps.wxr' );

		// Correct front-page id according to import (ID changes), last
		$this->front( $options );

		return true;
	}
}
