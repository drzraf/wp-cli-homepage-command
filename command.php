<?php

/**
  * Copyright (C) 2019, raphaël.droz+floss@gmail.com
  *
  * This program is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.
  */

if ( ! class_exists( 'WP_CLI' ) ) {
	return;
}
if ( PHP_SAPI !== 'cli' ) {
	return;
}

/**
 * Export/Import a WordPress homepage as a zip archive
 *
 * The zip archive contains the necessary data : image files, posts, menu items and some options.
 * Commands still make some assumption about posts/post-type presence on the frontpage.
 */
class HomepageCommand extends \WP_CLI_Command {

	/**
	 * Dump the dependencies of the homepage
	 *
	 * ZipArchive extension and wp-cli "export" command must be availble.
	 *
	 * ## OPTIONS
	 *
	 * [<filename>]
	 * : Where the store the archive (stdout if omitted).
   *
	 * @example
	 *  wp homepage dump file.zip
	 *
	 */
	function dump( $args, $assoc_args ) {
		require( __DIR__ . '/vendor/autoload.php' );

		list( $name ) = $args;
		$filename     = $name ?: '-';
		$ret          = Homepage\Exporter::dump( $filename );
		return ! $ret;
	}

	/**
	 * Load a homepage from an archive
	 *
	 * ZipArchive and wxr-importer plugin must be be available and enabled.
	 *
	 * ## OPTIONS
	 *
	 * <filename>
	 * : Where to load the archive from.
   *
	 * @example
	 *  wp homepage load file.zip
	 *
	 */
	function load( $args, $assoc_args ) {
		list( $name ) = $args;
		$filename     = $name ?: 'homepage.zip';
		if ( ! file_exists( $filename ) ) {
			throw new Exception( "file $filename not found" );
		}
		if ( ! file_exists( '/.dockerenv' ) && getenv( 'I_WANT_TO_IMPORT' ) !== 'yes!' || get_current_user() !== 'root' ) {
			exit( 'Forbidden (extra-security)' );
		}

		$ret = Homepage\Importer::load( $filename );
		return ! $ret;
	}
}

\WP_CLI::add_command( 'homepage', 'HomepageCommand' );
